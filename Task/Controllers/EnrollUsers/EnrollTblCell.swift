//
//  EnrollTblCell.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import UIKit

class EnrollTblCell: UITableViewCell {
    @IBOutlet weak var tfInput : UITextField!
    @IBOutlet weak var dateView : UIView!
    @IBOutlet weak var lblDateVal : UILabel!
    @IBOutlet weak var imgViewProfile : UIImageView!
    
    weak var parentVC : EnrollVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK:- PrepareCell

extension EnrollTblCell{
    func prepareCell(index: Int,data: EnrollModel){
        switch data.cellType {
        case .txtFieldCell:
            tfInput.placeholder = data.placeHolderName
            tfInput.keyboardType = data.keyboardType
            tfInput.returnKeyType = data.keyReturnType
            tfInput.tag = index
            tfInput.borderStyle = .roundedRect
            //tfInput.layer.cornerRadius = 10
            tfInput.layer.borderWidth = 1.0
            tfInput.layer.borderColor = UIColor(red: 0.29, green: 0.60, blue: 0.81, alpha: 1.00).cgColor
            self.addDoneButtonOnKeyboard()
            
        case .dateCell:
            //dateView.layer.cornerRadius = 10
            dateView.layer.borderWidth = 1.0
            dateView.layer.borderColor = UIColor(red: 0.29, green: 0.60, blue: 0.81, alpha: 1.00).cgColor
            lblDateVal.text = data.value == "" ? "Date" : data.selectedDate
            
        case .profileSetupCell:
            imgViewProfile.image = data.image == nil ? UIImage(named: "ic_placeholder") : data.image
            imgViewProfile.layer.cornerRadius = 40
        default:
            break
        }
    }
}


//MARK:- TextField Action Methods
extension EnrollTblCell{
    @IBAction func tfInputDidChange (_ textField : UITextField){
        let str = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        guard let text = str else {return}
        parentVC.objModel.arrData[textField.tag].value = text
    }
}


//MARK:- TextField Delegate Methods
extension EnrollTblCell: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .next {
            if let index = parentVC.tblView.indexPath(for: self) {
                if let cell = parentVC.getFormIndex(row: index.row + 1) {
                    if index.row == 2{
                        textField.resignFirstResponder()
                    }else{
                        cell.tfInput.becomeFirstResponder()
                    }
                } else {
                    textField.resignFirstResponder()
                }
            }
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
}

//MARK:- Keyboard Done Button on Number Pad
extension EnrollTblCell{
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.black
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        if tfInput.tag == 7 || tfInput.tag == 8{
            self.tfInput.inputAccessoryView = doneToolbar
        }
    }
    
    @objc func doneButtonAction()
    {
        self.tfInput.resignFirstResponder()
    }
}


