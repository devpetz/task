//
//  EnrollVC.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class EnrollVC: UIViewController {
    
    @IBOutlet weak var tblView : UITableView!
    
    var objModel = EnrollUsers()
    let db = Firestore.firestore()
    var validation = Validation()
    var userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}


//MARK:- Others Methods
extension EnrollVC{
    func setupUI(){
        objModel.prepareEnrollUI()
        tblView.allowsSelection = false
        tblView.separatorStyle = .none
        tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 15, right: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func getFormIndex(row: Int, section: Int = 0) -> EnrollTblCell? {
        if let comCell = tblView.cellForRow(at: IndexPath(row: row, section: section)) as? EnrollTblCell {
            return comCell
        }
        return nil
    }
}


//MARK:- TableView Delegate & DataSource Methods
extension EnrollVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objModel.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : EnrollTblCell
        
        cell = tblView.dequeueReusableCell(withIdentifier: objModel.arrData[indexPath.row].cellType.cellID, for: indexPath) as! EnrollTblCell
        cell.prepareCell(index: indexPath.row, data: objModel.arrData[indexPath.row])
        cell.parentVC = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return objModel.arrData[indexPath.row].cellType.cellHeight
    }
}


//MARK:- Set Profile Image

extension EnrollVC{
    @IBAction func btnChooseTapped(_ sender: UIButton){
        let actionSheet = UIAlertController(title: "", message: "Choose From", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { _ in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
}


//MARK:- ImagePicker Delegate Methods
extension EnrollVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImg = info[.editedImage] as? UIImage{
            objModel.arrData[0].image = pickedImg
            tblView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Add Users
extension EnrollVC{
    @IBAction func btnAddTapped(_ sender: UIButton){
        let result = objModel.validateData()
        if result.isValid{
            if let image = self.objModel.arrData[0].image{
                self.uploadMedia(image: image)
                self.addData()
            }else{
                self.showError(title: "Warning", msg: "Please Select ProfilePic with id")
                print("error")
            }
            
        }else{
            self.showError(title: "Warning", msg: result.error)
        }
    }
    
    
    
    func addData(){
        var ref : DocumentReference!
        ref = db.collection("users").addDocument(data: objModel.prepareUserData()){error in
            if let err = error{
                print("Error adding Document \(err)")
            }else{
                print("success \(ref.documentID)")
                self.showError(title: "Success!", msg: "User Added Successfully")
            }
        }
    }
    
    func uploadMedia(image : UIImage) {
        let imageName = UUID().uuidString
        let storageRef = Storage.storage().reference().child(imageName + ".png")
        let compressionData = image.jpegData(compressionQuality: 1.0)
        if let uploadData = compressionData {            
            storageRef.putData(uploadData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error")
                }else{
                    storageRef.child(imageName).downloadURL { (url, error) in
                        guard error == nil, let imgUrl = url else{
                            return
                        }
                        let urlString = imgUrl.absoluteString
                        print(urlString)
                        UserDefaults.standard.set(urlString, forKey: "imageUrl")
                    }
                }
            }
        }
    }
}

//MARK:- Show Alert
extension EnrollVC{
    func showError(title: String,msg: String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


//MARK:- DatePicker Tapped
extension EnrollVC{
    @IBAction func btnDatebtnTapped(_ sender: UIButton){
        let datepicker = DDatePicker.instantiateViewFromNib(withView: self.view)
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        datepicker.selectionBlock = {date in
            let currSelectedDate = df.string(from: date)
            self.objModel.arrData[3].selectedDate = currSelectedDate
            self.objModel.arrData[3].value = "\(date.getAge())"
            self.tblView.reloadData()
        }
    }
}
