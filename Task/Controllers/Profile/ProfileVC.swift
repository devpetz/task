//
//  ProfileVC.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {
    @IBOutlet weak var scrollView : UIScrollView!
    @IBOutlet var btnTabs : [UIButton]!
    @IBOutlet weak var lblLeadingConstraint : NSLayoutConstraint!
    var id = ""
    
    var enrollVC: EnrollVC? {
           return self.children.last as? EnrollVC
       }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        prepareUI()
    }
}

//MARK:- PrepareUI
extension ProfileVC{
    func prepareUI(){
        self.view.layoutIfNeeded()
        self.setSelectedIndex(index: 0)
    }
}

//MARK:- Segue Methods
extension ProfileVC{
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "usersVC"{
            let usersVC = segue.destination as! UsersVC
        
        }else if segue.identifier == "enrollVC"{
            let enrollVC = segue.destination as! EnrollVC
        }
    }
}

//MARK:- Other Methods
extension ProfileVC{
    @IBAction func btnTabsTapped(_ sender: UIButton){
        let currPage = CGFloat(sender.tag) * _screenSize.width
        scrollView.scrollRectToVisible(CGRect(x: currPage, y: 0, width: _screenSize.width, height: _screenSize.height), animated: true)
    }
    
    func setSelectedIndex(index: Int){
        for (idx,btn) in self.btnTabs.enumerated(){
            btn.setTitleColor( idx == index ? #colorLiteral(red: 0.2901960784, green: 0.6, blue: 0.8078431373, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
        }
        
        let btnsWidth = btnTabs[index].frame.size.width
        self.lblLeadingConstraint.constant = CGFloat(index) * btnsWidth
        self.view.layoutIfNeeded()
    }
}

//MARK:- ScrollView Delegate Methods
extension ProfileVC : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
        let currPage = scrollView.currentPage
        self.setSelectedIndex(index: currPage)
    }
}
