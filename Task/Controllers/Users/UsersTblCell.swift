//
//  UsersTblCell.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import UIKit

class UsersTblCell: UITableViewCell {
    @IBOutlet weak var imgUserProfileView : UIImageView!
    @IBOutlet weak var lblFullName : UILabel!
    @IBOutlet weak var lblUserDetails : UILabel!
    @IBOutlet weak var btnDelete : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


//MARK:- PrepareCell
extension UsersTblCell{
    func prepareCell(data: Users){
        imgUserProfileView.image = data.imageName
        lblFullName.text = data.fullName
        lblUserDetails.text = data.fullDetails
    }
}
