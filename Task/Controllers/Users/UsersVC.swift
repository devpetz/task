//
//  UsersVC.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class UsersVC: UIViewController {
    @IBOutlet weak var usersTable: UITableView!
    
    var arrUsers : [Users] =  []
    
    var dbRef = Firestore.firestore()
    var index : Int = 0
    var indexP : [IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        usersTable.reloadData()
        
    }
}

//MARK:- Othres Methods
extension UsersVC{
    func prepareUI(){
        getData()
        usersTable.allowsSelection = false
        usersTable.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
    }
    
    @IBAction func btnDeleteTapped(_ sender: UIButton){
        self.deleteData(id: sender.tag, docId: arrUsers[index].id, indexpath: indexP)
    }
}

//MARK:- Retrive Data from FireStore
extension UsersVC{
    func getData(){
        arrUsers = []
        
        dbRef.collectionGroup("users").getDocuments { (query, error) in
            if let err = error{
                print(err.localizedDescription)
            }else{
                if let documents = query?.documents{
                    for data in documents{
                        let docId = data.documentID
                        let arr = data.data()
                        if let fName = arr["firstname"] as? String,
                            let lName = arr["lastname"] as? String,
                            let city = arr["city"] as? String,
                            let gender = arr["gender"] as? String{
                            let objData = Users(imgname: UIImage.add, firstname: fName, lastname: lName, age: "23", city: city, gender: gender, id: docId)
                            self.arrUsers.append(objData)
                            self.usersTable.reloadData()
                            
                        }
                    }
                    
                }
            }
        }
    }
    
    
}

//MARK:- Delete Data from FireStore
extension UsersVC{
    func deleteData(id: Int, docId: String, indexpath :[IndexPath]){
        dbRef.collection("users").document(docId).delete(){ err in
            if let error = err{
                print(error.localizedDescription)
            }else{
                print("Success Deleted")
                self.arrUsers.remove(at: id)
                self.usersTable.deleteRows(at: indexpath, with: .automatic)
                self.usersTable.reloadData()
            }
        }
    }
}


//MARK:- TableView Delegate & DataSource Methods
extension UsersVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UsersTblCell
        
        cell = usersTable.dequeueReusableCell(withIdentifier: "usersDetailCell", for: indexPath) as! UsersTblCell
        cell.prepareCell(data: arrUsers[indexPath.row])
        cell.btnDelete.tag = indexPath.row
        indexP = [indexPath]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
}
