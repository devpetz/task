//
//  Users.swift
//  Task
//
//  Created by Devang on 2/5/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import Foundation
import UIKit

class Users{
    var imageName : UIImage
    var firstName : String
    var lastName : String
    var age : String
    var city : String
    var gender : String
    var id : String
    
//    var getImage : UIImage?{
//        return UIImage(named: imageName)
//    }
    var fullName : String{
        return firstName + " \(lastName)"
    }
    
    var fullDetails : String{
        return age + " |" + gender + " |" + city
    }
    
    init(imgname : UIImage, firstname: String,lastname : String, age: String, city : String, gender : String,id: String) {
        self.imageName = imgname
        self.firstName = firstname
        self.lastName = lastname
        self.age = age
        self.city = city
        self.gender = gender
        self.id = id
    }
}
