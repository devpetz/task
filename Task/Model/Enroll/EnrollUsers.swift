//
//  EnrollUsers.swift
//  Task
//
//  Created by Devang on 2/4/21.
//  Copyright © 2021 Devang Lakhani. All rights reserved.
//

import Foundation
import UIKit

enum EnumEnrollUsers{
    case profileSetupCell
    case txtFieldCell
    case dateCell
    case btnCell
    
    var cellID: String{
        switch self {
        case .profileSetupCell:
            return "setProfilePicCell"
        case .txtFieldCell:
            return "textFieldCell"
        case .btnCell :
            return "btnCell"
        case .dateCell:
            return "dateCell"
        }
    }
    
    var cellHeight : CGFloat{
        switch self {
        case .profileSetupCell:
            return 165
        case .txtFieldCell:
            return 75
        case .btnCell,.dateCell:
            return 73
        }
    }
}

struct EnrollModel {
    var placeHolderName : String = ""
    var keyboardType : UIKeyboardType = .default
    var keyReturnType: UIReturnKeyType = .default
    var cellType : EnumEnrollUsers = .profileSetupCell
    var value : String = ""
    var imageUrl : URL?
    var image : UIImage?
    var webKeyName: String = ""
    var selectedDate : String = ""
}

class EnrollUsers{
    var arrData : [EnrollModel] = []
    
    func prepareEnrollUI(){
        var f0 = EnrollModel()
        f0.cellType = .profileSetupCell
        f0.webKeyName = "profilePic"
        self.arrData.append(f0)
        
        var f1 = EnrollModel()
        f1.placeHolderName = "First Name"
        f1.keyboardType = .default
        f1.keyReturnType = .next
        f1.cellType = .txtFieldCell
        f1.webKeyName = "firstname"
        self.arrData.append(f1)
        
        var f2 = EnrollModel()
        f2.placeHolderName = "Last Name"
        f2.keyboardType = .default
        f2.keyReturnType = .next
        f2.cellType = .txtFieldCell
        f2.webKeyName = "lastname"
        self.arrData.append(f2)
        
        var f10 = EnrollModel()
        f10.webKeyName = "age"
        f10.cellType = .dateCell
        self.arrData.append(f10)
        
        var f3 = EnrollModel()
        f3.placeHolderName = "Gender"
        f3.keyboardType = .default
        f3.keyReturnType = .next
        f3.cellType = .txtFieldCell
        f3.webKeyName = "gender"
        self.arrData.append(f3)
        
        var f4 = EnrollModel()
        f4.placeHolderName = "Country"
        f4.keyboardType = .default
        f4.keyReturnType = .next
        f4.webKeyName = "country"
        f4.cellType = .txtFieldCell
        self.arrData.append(f4)
        
        var f5 = EnrollModel()
        f5.placeHolderName = "State"
        f5.keyboardType = .default
        f5.keyReturnType = .next
        f5.webKeyName = "state"
        f5.cellType = .txtFieldCell
        self.arrData.append(f5)
        
        var f6 = EnrollModel()
        f6.placeHolderName = "Home Town"
        f6.keyboardType = .default
        f6.keyReturnType = .next
        f6.cellType = .txtFieldCell
        f6.webKeyName = "city"
        self.arrData.append(f6)
        
        var f7 = EnrollModel()
        f7.placeHolderName = "Phone Number"
        f7.keyboardType = .phonePad
        f7.webKeyName = "phone"
        f7.cellType = .txtFieldCell
        self.arrData.append(f7)
        
        var f8 = EnrollModel()
        f8.placeHolderName = "TelePhone Number"
        f8.keyboardType = .phonePad
        f8.webKeyName = "telePhoneNumber"
        f8.cellType = .txtFieldCell
        self.arrData.append(f8)
        
        var f9 = EnrollModel()
        f9.cellType = .btnCell
        self.arrData.append(f9)
    }
    
    func prepareUserData() -> [String: Any] {
        var dict: [String: Any] = [:]
        for userModel in self.arrData {
            guard !userModel.webKeyName.isEmpty else {continue}
            
            if userModel.cellType == .profileSetupCell{
                dict[userModel.webKeyName] = userModel.imageUrl
            }else{
                dict[userModel.webKeyName] = userModel.value
            }
        }
        return dict
    }
    
    func validateData() -> (isValid: Bool, error: String) {
        var result: (isValid: Bool, error: String) = (true, "")
        for userData in self.arrData{
            if userData.value.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty{
                guard !userData.placeHolderName.isEmpty else {continue}
                result.isValid = false
                result.error = "Please Enter \(userData.placeHolderName)"
                return result
            }
        }
        return result
    }
}
